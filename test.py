from pickle import load
from re import X
from representations.models import SVD
from representations.statistical import Stat
from representations.sent_trans import BERTTransformer
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score

import pandas as pd
data = pd.read_csv('train.En.csv',encoding='utf-8')
data = data[~data.tweet.isna()]
X_train, X_test, y_train, y_test = train_test_split(data.tweet.to_list(),data.sarcastic.to_list(),test_size = 0.15)
stat = Stat().fit_transform(X_train)
clf = SVC()
clf.fit(stat,y_train)
print('STAT:',f1_score(y_train,clf.predict(stat)))
bert = BERTTransformer().fit_transform(X_train)
clf = SVC()
clf.fit(bert,y_train)
print('sBERT:',f1_score(y_train,clf.predict(bert)))
tmp_svd = SVD()
r_train = tmp_svd.fit_transform(X_train,100000,512)
clf = SVC()
clf.fit(r_train,y_train)
print('SVD:',f1_score(y_train,clf.predict(r_train)))



"""
        import pickle 

        with open('tmp.pkl','wb') as f:
            pickle.dump(tmp_svd, f)
        load_svd = pickle.load(open('tmp.pkl','rb'))

        clf = SVC()
        clf.fit(r_train,y_train)

        new_text = load_svd.transform(X_test) 
        y_my = clf.predict(new_text)
        print('LOADED:',f1_score(y_test,y_my))
        del load_svd
        new_text = tmp_svd.transform(X_test) 
        y_my = clf.predict(new_text)
        print('ORIG:  ',f1_score(y_test,y_my))
"""

